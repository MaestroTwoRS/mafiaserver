﻿var GlobalMethods = {
    GetUsersList: function () {
        $.ajax({
            url: '/api_admin/GetUserForAdmin',
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            type: 'GET', data: {},
            success: function (data) {
                GD.UsersTable = JSON.parse(data);
            },
            error: function (xhr, status, errorThrown) {
            }
        }); 
    }, 
    SelectUser: function (id) {
        router.push({ path: '/admin/users?id=' + id });  
        GD.User.SelectedUserId = id;
    },
    GetUndefinedBool: function (rout, query_param, param2) {
        try { if (rout.query[query_param] === param2) { return true; } } catch (e) { return false };
    },
    UserSelectionChanged: function (rout) { 
        if (rout.query['id'] == undefined) { return null; }
        GD.User.SelectedUserId = rout.query['id'];
    },
    SetVIP: function () {
        $.post("/api_admin/new_vip", { UserId: GD.User.SelectedUserId, BeginTime: GD.User.DateBeginVIP, EndTime: GD.User.DateEndVIP, Money: GD.User.PriceToVIPadd, DayPrice: GD.User.PriceDay, Paket: GD.User.TimeToVIPadd.value }, function (data) {
            alert(data);
        }); 
    }, 
    ExecuteTimeMoneyForVIP: function (date, viptime, money) { 
        if (date == undefined || date == null || viptime==null){return;}
        var date_ = new Date(date);
        var date_1 = new Date(date); 
        GD.User.DateBeginVIP = date_1.toLocaleString(); 
        var textDate; 
        switch (viptime) {
            case 1:
                date_.setMonth(date_.getMonth(date_) + 1);   
                GD.User.DateEndVIPtext=date_.getDate() + '.' + (date_.getMonth() + 1) + '.' + date_.getFullYear(); 
                GD.User.DateEndVIP = date_.toLocaleString(); 
                break;
            case 2:
                date_.setMonth(date_.getMonth(date_) + 2);
                date_.setDate(date_.getDate(date_) + 2); 
                GD.User.DateEndVIPtext = date_.getDate() + '.' + (date_.getMonth() + 1) + '.' + date_.getFullYear(); 
                GD.User.DateEndVIP = date_.toLocaleString();  
                break;
            case 3:
                date_.setMonth(date_.getMonth(date_) + 3);
                date_.setDate(date_.getDate(date_) + 5); 
                GD.User.DateEndVIPtext = date_.getDate() + '.' + (date_.getMonth() + 1) + '.' + date_.getFullYear(); 
                GD.User.DateEndVIP = date_.toLocaleString();  
                break; 
        }
        GD.User.DaysForVIP = Math.floor((date_.getTime() - date_1.getTime()) / (1000 * 60 * 60 * 24));
        GD.User.PriceDay = (money * viptime / GD.User.DaysForVIP).toFixed(3);
        //var N = 1;
        //date_.setMonth(date_.getMonth() + N);
        //alert(date_);
        
    }

    
};  
        

