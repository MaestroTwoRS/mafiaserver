﻿getCookie=function (name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
};

var GlobalMethods = {

    Register: function (mail, password, name, captcha) {  
        $.post("api/register", { Mail: mail, Password: password, Name:name, Captcha: captcha }, function (data) {
            if (data === "1") {
                GD.Register.RegisterSuccess=true; 
            } else {
                if (data === "2"){ 
                    GD.Register.ErrorMessage = "Эта почта уже занята";
                } else { 
                    GD.Register.ErrorMessage = data;
                }
                 
            }
        });
    },
    Login: function (login, password) { 
        $.post("api/auth", { Email: login.toLowerCase(), Password: password}, function (data) {
            if (data == "1") {
                GD.Auth.IsLoginFalse = 0;
                window.location = "/my_page";
            } else { 
                GD.Auth.IsLoginFalse = 1; 
            }
        });
    } 
};
        

