﻿function Reconnect() {
    var ws = new WebSocket("ws://127.0.0.1/ws");
    ws.onopen = function () {
        GD.WS.Status = true;
        GD.WS.StatusWord = "Подключен"; 
        ws.send("111");
    };

    ws.onerror = function (evt) {
        ws.close();
    };

    ws.onmessage = function (evt) {
        var received_msg = evt.data;
        //alert("Message received = " + received_msg);
    };
    ws.onclose = function () {
        GD.WS.Status = false;
        GD.WS.StatusWord = "Отключен. Переподключение.....";
        setTimeout(Reconnect, 1000);
    };
};


//Reconnect();

